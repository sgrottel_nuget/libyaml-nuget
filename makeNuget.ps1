#
# Builds libyaml.nuget
#
param([int]$build = -1)
$ErrorActionPreference = "Stop"

# Collect information from `_install`
$instDir = [System.IO.Path]::Combine($PSScriptRoot, "_install")
if ($build -lt 0) {
	$build = gc "$instDir\buildversion.txt" | Out-String
	Write-Host "Build version: $build"
}
$toolsets = gci $instDir -Directory -Filter "v*" | Where-Object {
	-not ((Test-Path @(
		[System.IO.Path]::Combine($_.FullName, "x64\Release"),
		[System.IO.Path]::Combine($_.FullName, "x64\Debug"),
		[System.IO.Path]::Combine($_.FullName, "Win32\Release"),
		[System.IO.Path]::Combine($_.FullName, "Win32\Debug")
	)) -contains $false)
} | foreach { [int]($_.Name.Substring(1)) } | sort -Descending
$refToolset = $toolsets[0]

$cmakeVerFile = gc ([System.IO.Path]::Combine($instDir, "v$refToolSet\x64\Release\cmake\yamlConfigVersion.cmake")) | Out-String
if (($cmakeVerFile -match '(?m)set\(PACKAGE_VERSION\s+"([0-9\.]+)"\)') -ne $true) { throw "Failed to parse PACKAGE_VERSION" }
$version = $Matches[1] + "." + $build
Write-Host "Version: $version"


# Consistency check
$configs = @(
	"x64\Release",
	"x64\Debug",
	"Win32\Release",
	"Win32\Debug"
)
$toolsets | foreach {
	$toolset = $_
	$configs | foreach {
		$cmakeVerFile = gc ([System.IO.Path]::Combine($instDir, "v$toolset\$_\cmake\yamlConfigVersion.cmake")) | Out-String
		if (($cmakeVerFile -match '(?m)set\(PACKAGE_VERSION\s+"([0-9\.]+)"\)') -ne $true) { throw "Failed to parse PACKAGE_VERSION" }
		#Write-Host "v$toolset\$_ --> $($Matches[1])"
		if ($version -ne $Matches[1] + "." + $build) { throw "v$toolset\$_ does not match version: $($Matches[1])" }
	}
}


# Build nuspec
copy -path ([System.IO.Path]::Combine($PSScriptRoot, "_clone\License")) -Destination "$instDir\License.txt"
[xml]$nuspec = gc ([System.IO.Path]::Combine($PSScriptRoot, "res\libyaml.nuspec"))
[System.Xml.XmlNamespaceManager]$ns = $nuspec.NameTable
$ns.AddNamespace("Any", $nuspec.DocumentElement.NamespaceURI)

$nuspec.SelectSingleNode("//Any:metadata/Any:version", $ns).InnerText = $version

$copyrightStr = gc "$instDir\License.txt" -Encoding "utf8" | Where-Object { $_.ToString().Contains("Copyright") } | Out-String
$nuspec.SelectSingleNode("//Any:metadata/Any:copyright", $ns).InnerText = $copyrightStr

$nuspec.SelectSingleNode("//Any:files/Any:file[@target='build\native\include']", $ns).SetAttribute("src", "v$refToolSet\x64\Release\include\*.h")

$files = $nuspec.SelectSingleNode("//Any:files", $ns)
$toolsets | foreach {
	$toolset = $_
	$configs | foreach {
		$config = $_
		$lib = $nuspec.CreateElement("file", "http://schemas.microsoft.com/packaging/2011/10/nuspec.xsd")
		$lib.SetAttribute("src", "v$toolset\$config\lib\yaml.lib")
		$lib.SetAttribute("target", "build\native\lib\v$toolset\$config")
		$files.AppendChild($lib) | Out-Null
		if (Test-Path "$instDir\v$toolset\$config\lib\yaml.pdb") {
			$lib = $nuspec.CreateElement("file", "http://schemas.microsoft.com/packaging/2011/10/nuspec.xsd")
			$lib.SetAttribute("src", "v$toolset\$config\lib\yaml.pdb")
			$lib.SetAttribute("target", "build\native\lib\v$toolset\$config")
			$files.AppendChild($lib) | Out-Null
		}
	}
}

$nuspec.Save("$instDir\libyaml.nuspec")


# Build libyaml.targets
[xml]$targets = gc ([System.IO.Path]::Combine($PSScriptRoot, "res\libyaml.targets"))
[System.Xml.XmlNamespaceManager]$ns = $targets.NameTable
$ns.AddNamespace("Any", $targets.DocumentElement.NamespaceURI)

$choose = $targets.SelectSingleNode("//Any:Choose[not(*)]", $ns)
$toolsets | foreach {
	$tswe = $targets.CreateElement("When", "http://schemas.microsoft.com/developer/msbuild/2003")
	$choose.AppendChild($tswe).SetAttribute("Condition", "'`$(PlatformToolsetVersion)' == '$_'")
	$tspge = $targets.CreateElement("PropertyGroup", "http://schemas.microsoft.com/developer/msbuild/2003")
	$tswe.AppendChild($tspge).SetAttribute("Condition", "'`$(libyamlPlatformToolset)' == ''")
	$tspge.AppendChild($targets.CreateElement("libyamlPlatformToolset", "http://schemas.microsoft.com/developer/msbuild/2003")).InnerText = "v$_"
}

$targets.Save("$instDir\libyaml.targets")


# Build libyaml-propertiesui.xml
[xml]$propertiesui = gc ([System.IO.Path]::Combine($PSScriptRoot, "res\libyaml-propertiesui.xml"))
[System.Xml.XmlNamespaceManager]$ns = $propertiesui.NameTable
$ns.AddNamespace("Any", $propertiesui.DocumentElement.NamespaceURI)
$toolsetEnum = $propertiesui.SelectSingleNode("//Any:EnumProperty[@DisplayName='Toolset']", $ns)
$toolsets | foreach {
	$tse = $propertiesui.CreateElement("EnumValue", "clr-namespace:Microsoft.Build.Framework.XamlTypes;assembly=Microsoft.Build.Framework")
	$tse.SetAttribute("Name", "v$_")
	$tse.SetAttribute("DisplayName", "v$_")
	$toolsetEnum.AppendChild($tse) | Out-Null
}
$propertiesui.Save("$instDir\libyaml-propertiesui.xml")


# pack
nuget pack "$instDir\libyaml.nuspec"
