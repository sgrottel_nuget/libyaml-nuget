param(
	[String]$appVeyorApiToken = "yjy40es2t83sr4mcxpa7",
	[String]$commitID)
$ErrorActionPreference = "Stop"

# Ensure commitId is known
if (-not $commitID) {
	$commitID = git rev-parse HEAD
	Write-Host "Selected commit: $commitID"
}

# clean bin
$instPath = "$PSScriptRoot\_install"
if (!(test-path $instPath)) { New-Item -ItemType Directory -Force -Path $instPath | Out-Null }
if (test-path $instPath) { remove-item "$instPath\*" -Recurse -Force }
$clonePath = "$PSScriptRoot\_clone"
if (!(test-path $clonePath)) { New-Item -ItemType Directory -Force -Path $clonePath | Out-Null }
if (test-path $clonePath) { remove-item "$clonePath\*" -Recurse -Force }

# Fetch available builds
# Setup
$appVeyorApi = 'https://ci.appveyor.com/api'
$headers = @{}
$headers['Authorization'] = "Bearer $appVeyorApiToken"
$headers['Content-type'] = 'application/json'
# Collect last 100 builds
$history = Invoke-RestMethod -Uri "$appVeyorApi/projects/s_grottel/libyaml-nuget/history?recordsNumber=100" -Headers $headers -Method Get
# Find build for this commit
$builds = $history.builds | Where-Object -Property 'status' -eq -value 'success' | Where-Object -Property 'commitId' -eq -Value $commitID
$buildversion = $builds[0].version
Set-Content -Path "$instPath\buildversion.txt" -Value $buildversion
# request build details
$build = Invoke-RestMethod -Uri "$appVeyorApi/projects/s_grottel/libyaml-nuget/build/$buildversion" -Headers $headers -Method Get
$jobs = $build.build.jobs | Where-Object -Property 'status' -eq -value 'success'
$artCnt = 0
$jobs | foreach {
	Write-Host "Downloading: $($_.name)"
	Write-Host "[$($_.jobId)]"
	$jobId = $_.jobId
	$artifacts = Invoke-RestMethod -Method Get -Uri "$appVeyorApi/buildjobs/$jobId/artifacts" -Headers $headers
	$artifacts | Select-Object -ExpandProperty 'fileName' | foreach {
		Write-Host "`t$_"
		$localPath = "$PSScriptRoot\$_"
		$localDir = [System.IO.Path]::GetDirectoryName($localPath)
		if (!(test-path $localDir)) { New-Item -ItemType Directory -Force -Path $localDir | Out-Null }
		Invoke-RestMethod -Method Get -Uri "$appVeyorApi/buildjobs/$jobId/artifacts/$_" `
			-OutFile $localPath -Headers @{ "Authorization" = "Bearer $appVeyorApiToken" }
		$artCnt++
	}
}

# Done
Write-Host "Downloaded $artCnt artifacts"
