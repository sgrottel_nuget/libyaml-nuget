# libYAML Nuget

![Logo](res/logo.png)

Scripting provided to build a nuget for libYAML as static library.

* Source repository: https://github.com/yaml/libyaml
* Project home page: https://go.grottel.net/nuget/libYAML
* Package script repository: https://bitbucket.org/sgrottel_nuget/libyaml-nuget

## Update

* Edit `.\libyaml_reference.ps1` for an update of the source files.
* A push to the bitbucket repository will trigger Appveyor builds:
* [![Build status](https://ci.appveyor.com/api/projects/status/ptpr6rm4upna8jfu?svg=true)](https://ci.appveyor.com/project/s_grottel/libyaml-nuget)
[![Build status](https://ci.appveyor.com/api/projects/status/ptpr6rm4upna8jfu/branch/master?svg=true)](https://ci.appveyor.com/project/s_grottel/libyaml-nuget/branch/master)
* As soon as the builds are all completed, run `.\collectArtifactos.ps1` do download the build artifacts for all targets to your local machine.
* Then, run `.\makeNuget.ps1` to pack the downloaded artifacts into a nupkg file.
* It is recommended to test that file with a small local project.
* If you are satisfied with the results, publish the nupkg to your nuget stream.
