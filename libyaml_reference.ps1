#
# configure which libyaml version to build and package
#
$libyaml_git_clone_url = "https://github.com/yaml/libyaml.git"
$libyaml_git_checkout = "0.2.5"
